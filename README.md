# libsahtrace

Small and flexible library to enable tracing and logging

This library is created to provide a simple and flexible system wide tracing and debuging functionality.
The library has some compile time options:
 * Disable log and tracing completely.
 * Configure a default trace level
 * Use colors in trace messages
The library also has runtime options to configure tracelevels dynamically.
The library supports tracezones to group log statements.

