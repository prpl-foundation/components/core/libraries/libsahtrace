export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export DOCDIR ?= $(D)/usr/share/doc/libsahtrace
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = libsahtrace

compile:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C doc clean

install:
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)/debug
	$(INSTALL) -D -p -m 0644 include/debug/*.h $(D)$(INCLUDEDIR)/debug/
	$(INSTALL) -D -p -m 0755 src/$(COMPONENT).so $(D)/lib/$(COMPONENT).so.$(PV)
	ln -sfr $(D)/lib/$(COMPONENT).so.$(PV) $(D)/lib/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config.pc $(PKG_CONFIG_LIBDIR)/sahtrace.pc

.PHONY: compile clean install
